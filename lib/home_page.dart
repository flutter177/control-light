import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_realtime/net/check_internet_connection.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _database = FirebaseDatabase.instance.ref();
  bool status1 = false;
  bool status2 = false;
  bool status3 = false;

  bool set1 = false;
  bool set2 = false;
  bool set3 = false;

  bool hasIntetnet = true;

  late bool check;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getInternet();
    _activeListeners();
  }

  void getInternet() async {
    bool value = await InternetConnection.isConnected();
    hasIntetnet = value;
    if (!hasIntetnet) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No Internet Connection!"),
      ));
    }
  }

  void _activeListeners() {
    _database.child('/myhome').onValue.listen((event) {
      print(event.snapshot.value);
      setState(() {
        final data = event.snapshot.value;
        status1 = (data as Map)['light1'] == "ON" ? true : false;
        status2 = (data as Map)['light2'] == "ON" ? true : false;
        status3 = (data as Map)['light3'] == "ON" ? true : false;
        set1 = status1;
        set2 = status2;
        set3 = status3;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final myHomeRef = _database.child('/myhome');
    return Scaffold(
        appBar: AppBar(
          title: Text("Control Light"),
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        ),
        body: Column(
          children: [
            Center(
              child: Text(
                'My Home',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('Light 1'),
                        status1 ? lightOn() : lightOff(),
                        Switch(
                            value: set1,
                            onChanged: (value) async {
                              print('value = $value');
                              set1 = value;
                              getInternet();
                              if (hasIntetnet) {
                                if (value) {
                                  try {
                                    await myHomeRef.update({'light1': 'ON'});
                                  } catch (e) {
                                    print('get an error ${e.toString()}');
                                  }
                                } else {
                                  try {
                                    await myHomeRef.update({'light1': 'OFF'});
                                  } catch (e) {
                                    print('get an error ${e.toString()}');
                                  }
                                }
                                setState(() {});
                              }
                            }),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('Light 2'),
                        status2 ? lightOn() : lightOff(),
                        Switch(
                            value: set2,
                            onChanged: (value) async {
                              getInternet();
                              if (hasIntetnet) {
                                if (value) {
                                  try {
                                    await myHomeRef.update({'light2': 'ON'});
                                  } catch (e) {
                                    print('get an error ${e.toString()}');
                                  }
                                } else {
                                  try {
                                    await myHomeRef.update({'light2': 'OFF'});
                                  } catch (e) {
                                    print('get an error ${e.toString()}');
                                  }
                                }
                                setState(() {});
                              }
                            }),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('Light 3'),
                        status3 ? lightOn() : lightOff(),
                        Switch(
                            value: set3,
                            onChanged: (value) async {
                              getInternet();
                              if (hasIntetnet) {
                                if (value) {
                                  try {
                                    await myHomeRef.update({'light3': 'ON'});
                                  } catch (e) {
                                    print('get an error ${e.toString()}');
                                  }
                                } else {
                                  try {
                                    await myHomeRef.update({'light3': 'OFF'});
                                  } catch (e) {
                                    print('get an error ${e.toString()}');
                                  }
                                }
                                setState(() {});
                              }
                            }),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }

  Widget lightOn() {
    return Container(
      width: 50,
      height: 50,
      child: Image.asset(
        'assets/images/lamp_on.png',
        fit: BoxFit.cover,
      ),
    );
  }

  Widget lightOff() {
    return Container(
      decoration: BoxDecoration(color: Colors.grey),
      width: 50,
      height: 50,
      child: Image.asset(
        'assets/images/lamp_off.png',
        fit: BoxFit.cover,
      ),
    );
  }
}
